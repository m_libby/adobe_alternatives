# Alternatives to Adobe Products

<table>
  <caption>
    Alternatives to Adobe Products
  </caption>
  <col>
  <col>
  <colgroup span="6"></colgroup>
  <thead>
    <tr>
      <th scope="col">Adobe App</th>
      <th scope="col">Alternative</th>
      <th colspan="1" scope="colgroup">License</th>
      <th colspan="5" scope="colgroup">Platforms</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th rowspan="9" scope="rowgroup">Photoshop (Ps) - photography</th>
      <th scope="row">GIMP/Glimpse</th>
      <td>FOSS</td>
      <td>*nix</td>
      <td>Windows</td>
      <td>Mac OS</td>
      <td></td>
      <td></td>
    </tr>
    <tr>
      <th scope="row">Sumo Paint</th>
      <td>Free</td>
      <td>*nix</td>
      <td>Windows</td>
      <td>Mac OS</td>
      <td></td>
      <td></td>
    </tr>
    <tr>
      <th scope="row">Affinity Photo</th>
      <td>Purchase</td>
      <td></td>
      <td>Windows</td>
      <td>Mac OS</td>
      <td></td>
      <td>iOS</td>
    </tr>
    <tr>
      <th scope="row">Photoline</th>
      <td>Purchase</td>
      <td></td>
      <td>Windows</td>
      <td>Mac OS</td>
      <td></td>
      <td></td>
    </tr>
    <tr>
      <th scope="row">Photopea</th>
      <td>Free</td>
      <td>*nix</td>
      <td>Windows</td>
      <td>Mac OS</td>
      <td></td>
      <td></td>
    </tr>
    <tr>
      <th scope="row">Paint.NET</th>
      <td>Free</td>
      <td></td>
      <td>Windows</td>
      <td></td>
      <td></td>
      <td></td>
    </tr>
    <tr>
      <th scope="row">Pixelmator Pro</th>
      <td>Purchase</td>
      <td></td>
      <td></td>
      <td>Mac OS</td>
      <td></td>
      <td></td>
    </tr>
    <tr>
      <th scope="row">PaintShop Pro</th>
      <td>Free</td>
      <td></td>
      <td>Windows</td>
      <td></td>
      <td></td>
      <td></td>
    </tr>
    <tr>
      <th scope="row">Pixlr</th>
      <td>Free</td>
      <td>*nix</td>
      <td>Windows</td>
      <td>Mac OS</td>
      <td></td>
      <td></td>
    </tr>
  </tbody>

  <tbody>
    <tr>
      <th rowspan="13" scope="rowgroup">Photoshop (Ps) - painting</th>
      <th scope="row">Krita</th>
      <td>FOSS</td>
      <td>*nix</td>
      <td>Windows</td>
      <td>Mac OS</td>
      <td>Android</td>
      <td></td>
    </tr>
    <tr>
      <th scope="row">Clip studio paint</th>
      <td>Purchase</td>
      <td></td>
      <td>Windows</td>
      <td>Mac OS</td>
      <td>Android</td>
      <td>iOS</td>
    </tr>
    <tr>
      <th scope="row">Paint tool SAI</th>
      <td>Purchase</td>
      <td></td>
      <td>Windows</td>
      <td>Mac OS</td>
      <td></td>
      <td></td>
    </tr>
    <tr>
      <th scope="row">Procreate</th>
      <td>Purchase</td>
      <td></td>
      <td></td>
      <td></td>
      <td></td>
      <td>iOS</td>
    </tr>
    <tr>
      <th scope="row">Artstudio Pro</th>
      <td>Purchase</td>
      <td></td>
      <td></td>
      <td></td>
      <td></td>
      <td>iOS</td>
    </tr>
    <tr>
      <th scope="row">IbiPaint</th>
      <td>Free</td>
      <td></td>
      <td></td>
      <td></td>
      <td>Android</td>
      <td></td>
    </tr>
    <tr>
      <th scope="row">AD Sketchbook</th>
      <td>Free</td>
      <td></td>
      <td></td>
      <td></td>
      <td>Android</td>
      <td>iOS</td>
    </tr>
    <tr>
      <th scope="row">Firealpaca</th>
      <td>FOSS</td>
      <td></td>
      <td>Windows</td>
      <td>Mac OS</td>
      <td></td>
      <td></td>
    </tr>
    <tr>
      <th scope="row">Medibang</th>
      <td>Free</td>
      <td></td>
      <td>Windows</td>
      <td>Mac OS</td>
      <td>Android</td>
      <td></td>
    </tr>
    <tr>
      <th scope="row">Paintstorm</th>
      <td>Purchase</td>
      <td></td>
      <td>Windows</td>
      <td>Mac OS</td>
      <td></td>
      <td>iOS</td>
    </tr>
    <tr>
      <th scope="row">MyPaint</th>
      <td>FOSS</td>
      <td>*nix</td>
      <td>Windows</td>
      <td>Mac OS</td>
      <td></td>
      <td></td>
    </tr>
    <tr>
      <th scope="row">Clip studio paint</th>
      <td>Purchase</td>
      <td></td>
      <td>Windows</td>
      <td>Mac OS</td>
      <td>Android</td>
      <td>iOS</td>
    </tr>
    <tr>
      <th scope="row">Paint tool SAI</th>
      <td>Purchase</td>
      <td></td>
      <td>Windows</td>
      <td>Mac OS</td>
      <td></td>
      <td></td>
    </tr>
    </tbody>

  <tbody>
    <tr>
      <th rowspan="7" scope="rowgroup">Illustrator (Ai)</th>
      <th scope="row">Inkscape</th>
      <td>FOSS</td>
      <td>*nix</td>
      <td>Windows</td>
      <td>Mac OS</td>
      <td></td>
      <td></td>
    </tr>
    <tr>
      <th scope="row">Boxy SVG</th>
      <td>Free</td>
      <td>*nix</td>
      <td>Windows</td>
      <td>Mac OS</td>
      <td></td>
      <td></td>
    </tr>
    <tr>
      <th scope="row">Gravit Designer</th>
      <td>Freemium</td>
      <td>*nix</td>
      <td>Windows</td>
      <td>Mac OS</td>
      <td></td>
      <td></td>
    </tr>
    <tr>
      <th scope="row">Affinity Designer</th>
      <td>Purchase</td>
      <td></td>
      <td>Windows</td>
      <td>Mac OS</td>
      <td></td>
      <td>iOS</td>
    </tr>
    <tr>
      <th scope="row">Vectornator</th>
      <td>Purchase</td>
      <td></td>
      <td></td>
      <td>Mac OS</td>
      <td></td>
      <td>iOS</td>
    </tr>
    <tr>
      <th scope="row">Corel Draw</th>
      <td>Purchase</td>
      <td></td>
      <td>Windows</td>
      <td>Mac OS</td>
      <td></td>
      <td></td>
    </tr>
    <tr>
      <th scope="row">Xara Designer</th>
      <td>Free</td>
      <td></td>
      <td>Windows</td>
      <td></td>
      <td></td>
      <td></td>
    </tr>
  </tbody>

  <tbody>
    <tr>
      <th rowspan="1" scope="rowgroup">Live Trace</th>
      <th scope="row">Vector Magic</th>
      <td>Purchase</td>
      <td>*nix</td>
      <td>Windows</td>
      <td>Mac OS</td>
      <td></td>
      <td></td>
    </tr>
  </tbody>

  <tbody>
    <tr>
      <th rowspan="11" scope="rowgroup">Animate (An)</th>
      <th scope="row">Blender</th>
      <td>FOSS</td>
      <td>*nix</td>
      <td>Windows</td>
      <td>Mac OS</td>
      <td></td>
      <td></td>
    </tr>
    <tr>
      <th scope="row">Cacani</th>
      <td>Purchase</td>
      <td></td>
      <td>Windows</td>
      <td></td>
      <td></td>
      <td></td>
    </tr>
    <tr>
      <th scope="row">Toonboom</th>
      <td>Purchase</td>
      <td>*nix</td>
      <td>Windows</td>
      <td>Mac OS</td>
      <td></td>
      <td></td>
    </tr>
    <tr>
      <th scope="row">Clip Studio EX</th>
      <td>Purchase</td>
      <td></td>
      <td>Windows</td>
      <td>Mac OS</td>
      <td></td>
      <td></td>
    </tr>
    <tr>
      <th scope="row">Moho</th>
      <td>Purchase</td>
      <td></td>
      <td>Windows</td>
      <td>Mac OS</td>
      <td></td>
      <td></td>
    </tr>
    <tr>
      <th scope="row">Open Toonz</th>
      <td>FOSS</td>
      <td>*nix</td>
      <td>Windows</td>
      <td>Mac OS</td>
      <td></td>
      <td></td>
    </tr>
    <tr>
      <th scope="row">Wick Editor</th>
      <td>FOSS</td>
      <td></td>
      <td></td>
      <td></td>
      <td></td>
      <td></td>
    </tr>
    <tr>
      <th scope="row">Synfig</th>
      <td>FOSS</td>
      <td></td>
      <td></td>
      <td></td>
      <td></td>
      <td></td>
    </tr>
    <tr>
      <th scope="row">Krita</th>
      <td>FOSS</td>
      <td>*nix</td>
      <td>Windows</td>
      <td>Mac OS</td>
      <td>Android</td>
      <td></td>
    </tr>
    <tr>
      <th scope="row">Pencil 2D</th>
      <td>FOSS</td>
      <td>*nix</td>
      <td>Windows</td>
      <td>Mac OS</td>
      <td></td>
      <td></td>
    </tr>
    <tr>
      <th scope="row">Firealpaca</th>
      <td>FOSS</td>
      <td></td>
      <td>Windows</td>
      <td>Mac OS</td>
      <td></td>
      <td></td>
    </tr>
  </tbody>

  <tbody>
    <tr>
      <th rowspan="3" scope="rowgroup">InDesign (Id)</th>
      <th scope="row">Scribus</th>
      <td>FOSS</td>
      <td>*nix</td>
      <td>Windows</td>
      <td>Mac OS</td>
      <td></td>
      <td></td>
    </tr>
    <tr>
      <th scope="row">Affinity Publisher</th>
      <td>Purchase</td>
      <td></td>
      <td>Windows</td>
      <td>Mac OS</td>
      <td></td>
      <td></td>
    </tr>
    <tr>
      <th scope="row">QuarkXpress</th>
      <td>Purchase</td>
      <td></td>
      <td>Windows</td>
      <td>Mac OS</td>
      <td></td>
      <td></td>
    </tr>
  </tbody>

  <tbody>
    <tr>
      <th rowspan="7" scope="rowgroup">Premier Pro (Pr)</th>
      <th scope="row">Olive</th>
      <td>FOSS</td>
      <td>*nix</td>
      <td>Windows</td>
      <td>Mac OS</td>
      <td></td>
      <td></td>
    </tr>
    <tr>
      <th scope="row">Kdenlive</th>
      <td>FOSS</td>
      <td>*nix</td>
      <td>Windows</td>
      <td>Mac OS</td>
      <td></td>
      <td></td>
    </tr>
    <tr>
      <th scope="row">[OpenShot](https://www.openshot.org/)</th>
      <td>FOSS</td>
      <td>*nix</td>
      <td>Windows</td>
      <td>Mac OS</td>
      <td>Chrome OS</td>
      <td></td>
    </tr>
    <tr>
      <th scope="row">Davinci Resolve</th>
      <td>Frreemium</td>
      <td>*nix</td>
      <td>Windows</td>
      <td>Mac OS</td>
      <td></td>
      <td></td>
    </tr>
    <tr>
      <th scope="row">Sony Vegas</th>
      <td>Purchase</td>
      <td></td>
      <td>Windows ?</td>
      <td></td>
      <td></td>
      <td></td>
    </tr>
    <tr>
      <th scope="row">Final Cut Pro X</th>
      <td>Purchase</td>
      <td></td>
      <td></td>
      <td>Mac OS</td>
      <td></td>
      <td></td>
    </tr>
    <tr>
      <th scope="row">Kapwing</th>
      <td>Free/Sub</td>
      <td>*nix</td>
      <td>Windows</td>
      <td>Mac OS</td>
      <td></td>
      <td></td>
    </tr>
  </tbody>

  <tbody>
    <tr>
      <th rowspan="9" scope="rowgroup">Lightroom (Lr)</th>
      <th scope="row">RawTherapee</th>
      <td>FOSS</td>
      <td>*nix</td>
      <td>Windows</td>
      <td>Mac OS</td>
      <td></td>
      <td></td>
    </tr>
    <tr>
      <th scope="row">DarkTable</th>
      <td>FOSS</td>
      <td>*nix</td>
      <td>Windows</td>
      <td>Mac OS</td>
      <td></td>
      <td></td>
    </tr>
    <tr>
      <th scope="row">DigiKam</th>
      <td>FOSS</td>
      <td>*nix</td>
      <td>Windows</td>
      <td>Mac OS</td>
      <td></td>
      <td></td>
    </tr>
    <tr>
      <th scope="row">On1</th>
      <td>Purchase</td>
      <td></td>
      <td>Windows</td>
      <td>Mac OS</td>
      <td></td>
      <td></td>
    </tr>
    <tr>
      <th scope="row">FastRawViewer</th>
      <td>Purchase</td>
      <td>*nix</td>
      <td>Windows</td>
      <td>Mac OS</td>
      <td></td>
      <td></td>
    </tr>
    <tr>
      <th scope="row">Capture One</th>
      <td>Purchase</td>
      <td>*nix</td>
      <td>Windows</td>
      <td>Mac OS</td>
      <td></td>
      <td></td>
    </tr>
    <tr>
      <th scope="row">Dx0 PhotoLab</th>
      <td>Purchase</td>
      <td>*nix</td>
      <td>Windows</td>
      <td>Mac OS</td>
      <td></td>
      <td></td>
    </tr>
    <tr>
      <th scope="row">Luminar</th>
      <td>Purchase</td>
      <td>*nix</td>
      <td>Windows</td>
      <td>Mac OS</td>
      <td></td>
      <td></td>
    </tr>
    <tr>
      <th scope="row">Aurora</th>
      <td>Purchase</td>
      <td>*nix</td>
      <td>Windows</td>
      <td>Mac OS</td>
      <td></td>
      <td></td>
    </tr>
  </tbody>

  <tbody>
    <tr>
      <th rowspan="9" scope="rowgroup">Adobe XD</th>
      <th scope="row">Penpot</th>
      <td>FOSS</td>
      <td>*nix</td>
      <td>Windows</td>
      <td>Mac OS</td>
      <td></td>
      <td></td>
    </tr>
    <tr>
      <th scope="row">Draftoola</th>
      <td>FOSS</td>
      <td>*nix</td>
      <td>Windows</td>
      <td>Mac OS</td>
      <td></td>
      <td></td>
    </tr>
    <tr>
      <th scope="row">Figma</th>
      <td>Free/Sub</td>
      <td></td>
      <td></td>
      <td></td>
      <td></td>
      <td></td>
    </tr>
    <tr>
      <th scope="row">Framer</th>
      <td>Free/Sub</td>
      <td></td>
      <td></td>
      <td></td>
      <td></td>
      <td></td>
    </tr>
    <tr>
      <th scope="row">Sketch</th>
      <td>Purchase</td>
      <td></td>
      <td></td>
      <td>Mac OS</td>
      <td></td>
      <td></td>
    </tr>
    <tr>
      <th scope="row">Lunacy</th>
      <td>Purchase</td>
      <td></td>
      <td>Windows</td>
      <td>Mac OS</td>
      <td></td>
      <td></td>
    </tr>
    <tr>
      <th scope="row">Origami</th>
      <td>Purchase</td>
      <td></td>
      <td></td>
      <td>Mac OS</td>
      <td></td>
      <td></td>
    </tr>
    <tr>
      <th scope="row">Axure RP</th>
      <td>Purchase</td>
      <td>*nix</td>
      <td>Windows</td>
      <td>Mac OS</td>
      <td></td>
      <td></td>
    </tr>
    <tr>
      <th scope="row">Principle</th>
      <td>Purchase</td>
      <td></td>
      <td></td>
      <td>Mac OS</td>
      <td></td>
      <td></td>
    </tr>
  </tbody>

  <tbody>
    <tr>
      <th rowspan="14" scope="rowgroup">Audition (Au)</th>
      <th scope="row">Audacity</th>
      <td>FOSS</td>
      <td>*nix</td>
      <td>Windows</td>
      <td>Mac OS</td>
      <td></td>
      <td></td>
    </tr>
    <tr>
      <th scope="row">Ocenaudio</th>
      <td>Free</td>
      <td></td>
      <td>Windows</td>
      <td></td>
      <td></td>
      <td></td>
    </tr>
    <tr>
      <th scope="row">Davinci fairlight</th>
      <td>Freemium</td>
      <td>*nix</td>
      <td>Windows</td>
      <td>Mac OS</td>
      <td></td>
      <td></td>
    </tr>
    <tr>
      <th scope="row">iZotope RX</th>
      <td>Free</td>
      <td></td>
      <td>Windows</td>
      <td>Mac OS</td>
      <td></td>
      <td></td>
    </tr>
    <tr>
      <th scope="row">Cakewalk</th>
      <td>Free</td>
      <td></td>
      <td>Windows</td>
      <td></td>
      <td></td>
      <td></td>
    </tr>
    <tr>
      <th scope="row">Ardour</th>
      <td>FOSS</td>
      <td>*nix</td>
      <td>Windows</td>
      <td>Mac OS</td>
      <td></td>
      <td></td>
    </tr>
    <tr>
      <th scope="row">LMMS</th>
      <td>FOSS</td>
      <td>*nix</td>
      <td>Windows</td>
      <td>Mac OS</td>
      <td></td>
      <td></td>
    </tr>
    <tr>
      <th scope="row">Reaper</th>
      <td>Purchase</td>
      <td>*nix</td>
      <td>Windows</td>
      <td>Mac OS</td>
      <td></td>
      <td></td>
    </tr>
    <tr>
      <th scope="row">Traktion</th>
      <td>Freemium</td>
      <td>*nix</td>
      <td>Windows</td>
      <td>Mac OS</td>
      <td></td>
      <td></td>
    </tr>
    <tr>
      <th scope="row">Garageband</th>
      <td>Free</td>
      <td></td>
      <td></td>
      <td>Mac OS</td>
      <td></td>
      <td></td>
    </tr>
    <tr>
      <th scope="row">Pro Tools</th>
      <td>Freemium</td>
      <td></td>
      <td>Windows</td>
      <td>Mac OS</td>
      <td></td>
      <td></td>
    </tr>
    <tr>
      <th scope="row">Logic Pro</th>
      <td>Purchase</td>
      <td></td>
      <td></td>
      <td>Mac OS</td>
      <td></td>
      <td></td>
    </tr>
    <tr>
      <th scope="row">Live by Ableton</th>
      <td>Purchase</td>
      <td></td>
      <td>Windows</td>
      <td>Mac OS</td>
      <td></td>
      <td></td>
    </tr>
    <tr>
      <th scope="row">FL Studio</th>
      <td>Purchase</td>
      <td></td>
      <td>Windows</td>
      <td>Mac OS</td>
      <td>Android</td>
      <td></td>
    </tr>
  </tbody>

  <tbody>
    <tr>
      <th rowspan="4" scope="rowgroup">After Effects (Ae) - Compositing & FX</th>
      <th scope="row">Natron</th>
      <td>FOSS</td>
      <td>*nix</td>
      <td>Windows</td>
      <td>Mac OS</td>
      <td></td>
      <td></td>
    </tr>
    <tr>
      <th scope="row">Blender</th>
      <td>FOSS</td>
      <td>*nix</td>
      <td>Windows</td>
      <td>Mac OS</td>
      <td></td>
      <td></td>
    </tr>
    <tr>
      <th scope="row">Davinci Fusion</th>
      <td>Freemium</td>
      <td></td>
      <td>Windows</td>
      <td>Mac OS</td>
      <td></td>
      <td></td>
    </tr>
    <tr>
      <th scope="row">Hitfilm</th>
      <td>Freemium</td>
      <td></td>
      <td>Windows</td>
      <td>Mac OS</td>
      <td></td>
      <td></td>
    </tr>
  </tbody>
  
  <tbody>
    <tr>
      <th rowspan="6" scope="rowgroup">After Effects (Ae) - Motion Graphics</th>
      <th scope="row">Blender</th>
      <td>FOSS</td>
      <td>*nix</td>
      <td>Windows</td>
      <td>Mac OS</td>
      <td></td>
      <td></td>
    </tr>
    <tr>
      <th scope="row">Enve</th>
      <td>FOSS</td>
      <td>*nix</td>
      <td>Windows</td>
      <td>Mac OS</td>
      <td></td>
      <td></td>
    </tr>
    <tr>
      <th scope="row">Cavalry</th>
      <td>Free/Sub</td>
      <td>*nix</td>
      <td>Windows</td>
      <td>Mac OS</td>
      <td></td>
      <td></td>
    </tr>
    <tr>
      <th scope="row">Rive</th>
      <td>Free/Sub</td>
      <td></td>
      <td>Windows</td>
      <td>Mac OS</td>
      <td></td>
      <td></td>
    </tr>
    <tr>
      <th scope="row">Jitter</th>
      <td>Free/Sub</td>
      <td></td>
      <td>Windows</td>
      <td>Mac OS</td>
      <td></td>
      <td></td>
    </tr>
    <tr>
      <th scope="row">Fable</th>
      <td>Free</td>
      <td></td>
      <td>Windows</td>
      <td>Mac OS</td>
      <td></td>
      <td></td>
    </tr>
  </tbody>

  <tbody>
    <tr>
      <th rowspan="3" scope="rowgroup">Acrobat</th>
      <th scope="row">Xodo</th>
      <td>Free</td>
      <td></td>
      <td>Windows</td>
      <td>Mac OS</td>
      <td></td>
      <td></td>
    </tr>
    <tr>
      <th scope="row">PDF24</th>
      <td>Free</td>
      <td></td>
      <td>Windows</td>
      <td>Mac OS</td>
      <td></td>
      <td></td>
    </tr>
    <tr>
      <th scope="row">PDF-XChange</th>
      <td>Purchase</td>
      <td></td>
      <td>Windows</td>
      <td>Mac OS</td>
      <td></td>
      <td></td>
    </tr>
  </tbody>

  <tbody>
    <tr>
      <th rowspan="4" scope="rowgroup">Dreamweaver (Dr)</th>
      <th scope="row">Netbeans</th>
      <td>FOSS</td>
      <td>*nix</td>
      <td>Windows</td>
      <td>Mac OS</td>
      <td></td>
      <td></td>
    </tr>
    <tr>
      <th scope="row">VS Codium</th>
      <td>FOSS</td>
      <td>*nix</td>
      <td>Windows</td>
      <td>Mac OS</td>
      <td></td>
      <td></td>
    </tr>
    <tr>
      <th scope="row">VS Code</th>
      <td>Free</td>
      <td>*nix</td>
      <td>Windows</td>
      <td>Mac OS</td>
      <td></td>
      <td></td>
    </tr>
    <tr>
      <th scope="row">Blue Griffon</th>
      <td>Purchase</td>
      <td>*nix</td>
      <td>Windows</td>
      <td>Mac OS</td>
      <td></td>
      <td></td>
    </tr>
  </tbody>

  <tbody>
    <tr>
      <th rowspan="1" scope="rowgroup">Bridge</th>
      <th scope="row">XnView MP</th>
      <td>Free</td>
      <td>*nix</td>
      <td>Windows</td>
      <td>Mac OS</td>
      <td></td>
      <td></td>
    </tr>
    <tr>

</table>

Based on a graphic by [@xdanielArt](https://mastodon.gamedev.place/@xdanielArt)
